using System;
using PhantomForce.Flows;
using PhantomForce.GodotBridge;
using Victor.Yves;
using Object = Godot.Object;

namespace PhantomForce.Common
{
	interface IContext
	{
		RootNode Root { get; }

		void Connect(Object source, string signal, Action handler);

		void DeferAndForget(Action action);
		SceneTask<T> DeferState<T>(Action<SceneTask<T>> enterState);
		SceneTask<T> Defer<T>(Func<T> function);
		SceneTask<None> Defer(Action action);
		SceneTask<T> DeferFlow<T>(Func<Flow<T>> flow);

		void AddEventHandler(IEventHandler eventHandler);
		void RemoveEventHandler(IEventHandler eventHandler);
	}
}
