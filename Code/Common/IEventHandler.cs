using Godot;

namespace PhantomForce.Common
{
	interface IEventHandler
	{
		void FrameUpdate(float delta);
		void Step(float delta);
		void Input(InputEvent e);
	}
}
