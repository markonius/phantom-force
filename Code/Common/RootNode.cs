using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Godot;
using PhantomForce.GodotBridge;
using ProtoBuf.Meta;
using Victor.Yves;

namespace PhantomForce.Common
{
	abstract class RootNode : Node
	{
		public readonly Signals signals = new Signals();
		public readonly DeferredActions deferredActions = new DeferredActions();
		public abstract IContext Context { get; }

		readonly List<IEventHandler> eventHandlers = new List<IEventHandler>();

		public override void _Ready()
		{
			ConfigureSerializer();

			Task.Run(Start).LogErrors();
		}

		void ConfigureSerializer()
		{
			RuntimeTypeModel.Default.Add<None>();
			RuntimeTypeModel.Default.Add<Vector2>().Add(new[] { nameof(Vector2.x), nameof(Vector2.y) });
			RuntimeTypeModel.Default.Add<Vector3>().Add(new[] { nameof(Vector3.x), nameof(Vector3.y), nameof(Vector3.z) });
		}

		public TResult RunScene<TNode, TResult>(string path, Func<TNode, TResult> handle) where TNode : Node
		{
			var node = Loader.LoadScene<TNode>(path);
			deferredActions.Defer(() => AddChild(node)).Wait();
			TResult result = handle(node);
			deferredActions.Defer(() => RemoveChild(node)).Wait();
			node.Free();
			node.Dispose();
			return result;
		}

		public override void _Process(float delta)
		{
			deferredActions.Invoke();

			foreach (IEventHandler eventHandler in eventHandlers)
				eventHandler.FrameUpdate(delta);
		}

		public override void _PhysicsProcess(float delta)
		{
			foreach (IEventHandler eventHandler in eventHandlers)
				eventHandler.Step(delta);
		}

		public override void _UnhandledInput(InputEvent @event)
		{
			foreach (IEventHandler eventHandler in eventHandlers)
				eventHandler.Input(@event);
		}

		public void HandleSignal(int id)
		{
			signals.Handle(id);
		}

		public void AddEventHandler(IEventHandler eventHandler)
		{
			eventHandlers.Add(eventHandler);
		}

		public void RemoveEventHandler(IEventHandler eventHandler)
		{
			eventHandlers.Remove(eventHandler);
		}

		protected override void Dispose(bool disposing)
		{
			signals.Dispose();
			base.Dispose(disposing);
		}

		protected abstract void Start();
	}
}
