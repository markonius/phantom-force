using System;
using System.IO;
using ProtoBuf;
using static Victor.ExpressionKeywords;

namespace PhantomForce.Common.Network
{


	interface ITCPMessageHandler
	{
		void Handle(byte[] data);
	}

	struct TCPMessageHandler<T> : ITCPMessageHandler where T : notnull
	{
		readonly Action<T> handler;

		public TCPMessageHandler(Action<T> handler)
		{
			this.handler = handler;
		}

		public void Handle(byte[] data)
		{
			T message = Using(new MemoryStream(data), stream =>
			{
				stream.Position = 0;
				return Serializer.DeserializeWithLengthPrefix<T>(stream, PrefixStyle.Base128);
			});
			handler(message);
		}
	}
}
