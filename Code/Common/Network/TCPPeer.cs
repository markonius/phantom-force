using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using ProtoBuf;
using static Victor.ExpressionKeywords;


namespace PhantomForce.Common.Network
{
	// TODO: sync, maybe organize handlers in synced packages. Or just handle this within handlers?
	class TCPPeer
	{
		public delegate void Sender(long contentLength, Stream stream);

		static readonly ThreadLocal<byte[]> buffer = new ThreadLocal<byte[]>(() => new byte[1024]);

		readonly Sender tcpSend;

		readonly Dictionary<string, ITCPMessageHandler> messageHandlers = new Dictionary<string, ITCPMessageHandler>();

		public TCPPeer(Sender send)
		{
			tcpSend = send;
		}

		public void RegisterHandler<TMessage>(string path, Action<TMessage> handler) where TMessage : notnull
		{
			messageHandlers[path] = new TCPMessageHandler<TMessage>(handler);
		}

		public void RemoveHandler(string path)
		{
			messageHandlers.Remove(path);
		}

		public void Receive(byte[] data)
		{
			var packedMessage = Using(new MemoryStream(data), stream =>
			{
				stream.Position = 0;
				return Serializer.DeserializeWithLengthPrefix<TCPMessage>(stream, PrefixStyle.Base128);
			});
			ITCPMessageHandler handler = messageHandlers[packedMessage.path];
			handler.Handle(packedMessage.data);
		}

		public void Send<TMessage>(string path, TMessage message) where TMessage : notnull
		{
			byte[] buffer = TCPPeer.buffer.Value;
			TCPMessage packedMessage = Using(new MemoryStream(buffer, true), stream =>
			{
				stream.SetLength(0);
				stream.Position = 0;
				Serializer.SerializeWithLengthPrefix(stream, message, PrefixStyle.Base128);
				byte[] data = stream.ToArray();
				return new TCPMessage(path, data);
			});
			using (var stream = new MemoryStream(buffer, 0, 1024, true))
			{
				stream.SetLength(0);
				stream.Position = 0;
				Serializer.SerializeWithLengthPrefix(stream, packedMessage, PrefixStyle.Base128);
				stream.Position = 0;
				tcpSend(stream.Length, stream);
			}
		}
	}
}
