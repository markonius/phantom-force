using Godot;
using ProtoBuf;

namespace PhantomForce.Common.Network
{
	[ProtoContract]
	struct InputMessage
	{
		[ProtoMember(1)]
		public readonly byte playerID;
		[ProtoMember(2)]
		public readonly Vector2 direction;

		public InputMessage(byte playerID, Vector2 direction)
		{
			this.playerID = playerID;
			this.direction = direction;
		}
	}

	[ProtoContract]
	struct WorldStateMessage
	{
		[ProtoMember(1)]
		public readonly Vector2[] positions;

		public WorldStateMessage(Vector2[] positions)
		{
			this.positions = positions;
		}
	}
}
