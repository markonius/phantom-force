namespace PhantomForce.Common.Network
{
	struct TCPMessage
	{
		public readonly string path;
		public readonly byte[] data;

		public TCPMessage(string path, byte[] data)
		{
			this.path = path;
			this.data = data;
		}
	}
}
