using System;
using System.Collections.Generic;
using Godot;
using PhantomForce.Flows;
using PhantomForce.GodotBridge;
using Victor.Extensions;
using Victor.Yves;

namespace PhantomForce
{
	class DevLoader : Node
	{
		const string SERVER_SCENE = "res://Assets/Server/ServerRoot.tscn";
		const string CLIENT_SCENE = "res://Assets/Client/ClientRoot.tscn";

		[Export] public NodePath? sceneParentPath;
		[Export] public NodePath? visibleClientSelectorPath;

		Node? sceneParent;
		Node? server;
		readonly List<Node> clients = new List<Node>();
		OptionButton? visibleClientSelector;

		bool serverVisible = true;

		FlowState<None>? visibilityControl;

		public override void _Ready()
		{
			sceneParent = GetNode(sceneParentPath);
			visibleClientSelector = GetNode<OptionButton>(visibleClientSelectorPath);
			ControlVisibility().Ignore();
		}

		public override void _Process(float delta)
		{
			visibilityControl.NotNull().Complete(None.none);
		}

		async Flow<None> ControlVisibility()
		{
			while (true)
			{
				if (server != null)
					await ControlVisibility(server, () => serverVisible);

				for (int i = 0; i < clients.Count; i++)
				{
					bool IsVisible() => visibleClientSelector.NotNull().Selected == i;
					await ControlVisibility(clients[i], IsVisible);
					await ControlCameraEnabled(clients[i], IsVisible);
					await FlowState<None>.Create(s => visibilityControl = s);
				}
				await FlowState<None>.Create(s => visibilityControl = s);
			}
		}

		async Flow<None> ControlVisibility(Node node, Func<bool> getVisible)
		{
			foreach (Node child in node.GetChildren())
			{
				if (child is Spatial s)
					s.Visible = getVisible();
				else if (child is Control c)
					c.Visible = getVisible();
				else
					await ControlVisibility(child, getVisible);
				await FlowState<None>.Create(s => visibilityControl = s);
			}
			await FlowState<None>.Create(s => visibilityControl = s);
			return None.none;
		}

		async Flow<None> ControlCameraEnabled(Node node, Func<bool> getEnabled)
		{
			foreach (Node child in node.GetChildren())
			{
				if (child is Camera c)
				{
					c.Current = getEnabled();
					break;
				}
				else
				{
					await ControlCameraEnabled(child, getEnabled);
				}
				await FlowState<None>.Create(s => visibilityControl = s);
			}
			await FlowState<None>.Create(s => visibilityControl = s);
			return None.none;
		}

		public void SpawnServer()
		{
			server = Loader.LoadScene<Node>(SERVER_SCENE);
			sceneParent.NotNull().AddChild(server);
		}

		public void SpawnClient()
		{
			var node = Loader.LoadScene<Node>(CLIENT_SCENE);
			sceneParent.NotNull().AddChild(node);
			clients.Add(node);
			visibleClientSelector.NotNull().AddItem($"Client {clients.Count - 1}");
			visibleClientSelector!.Select(clients.Count - 1);
		}

		public void ToggleServerVisibility(bool on)
		{
			serverVisible = on;
		}
	}
}
