using System.Threading.Tasks;
using Godot;

namespace PhantomForce.GodotBridge
{
	static class TaskExtensions
	{
		public static Task<T> LogErrors<T>(this Task<T> task) =>
			task.ContinueWith(t =>
			{
				if (t.Exception != null)
					GD.PrintErr(t.Exception);
				return t.Result;
			});

		public static Task LogErrors(this Task task) =>
			task.ContinueWith(t =>
			{
				if (t.Exception != null)
					GD.PrintErr(t.Exception);
			});
	}
}
