using System;
using System.Collections.Generic;
using System.Threading;
using PhantomForce.Flows;
using Victor.Yves;

namespace PhantomForce.GodotBridge
{
	class DeferredActions
	{
		readonly List<Action> actions = new List<Action>();

		public void DeferAndForget(Action action)
		{
			lock (actions)
				actions.Add(action);
		}

		public SceneTask<T> DeferState<T>(Action<SceneTask<T>> enterState)
		{
			var task = new SceneTask<T>();
			void action() => enterState(task);
			DeferAndForget(action);
			return task;
		}

		public SceneTask<T> Defer<T>(Func<T> function)
		{
			void action(SceneTask<T> task)
			{
				T result = function();
				task.Complete(result);
			}
			return DeferState<T>(action);
		}

		public SceneTask<None> Defer(Action action)
		{
			None function()
			{
				action();
				return None.none;
			}
			return Defer(function);
		}

		public SceneTask<T> DeferFlow<T>(Func<Flow<T>> flow)
		{
			void action(SceneTask<T> task)
			{
				Flow<T> f = flow();
				f.OnCompleted(() =>
				{
					T result = f.GetResult();
					task.Complete(result);
				});
			}
			return DeferState<T>(action);
		}

		public void Invoke()
		{
			if (actions.Count > 0)
			{
				Action[] tempActions;
				lock (actions)
				{
					tempActions = actions.ToArray();
					actions.Clear();
				}
				foreach (Action action in tempActions)
					action();
			}
		}
	}

	class SceneTask<T>
	{
		readonly ManualResetEvent mainLock = new ManualResetEvent(false);

		T result = default!; // Should be nullable, but generics make this hard
		bool isComplete = false;

		public void Complete(T result)
		{
			if (isComplete)
				throw new Exception("SceneTask already complete.");
			this.result = result;
			isComplete = true;
			mainLock.Set();
		}

		public T Wait()
		{
			if (isComplete)
				return result;
			mainLock.WaitOne();
			mainLock.Dispose();
			return result;
		}
	}
}
