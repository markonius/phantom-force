using Godot;
using Victor.Extensions;

namespace PhantomForce.GodotBridge
{
	static class Loader
	{
		static readonly object loadingLock = new object();

		public static T Load<T>(string path) where T : Resource
		{
			lock (loadingLock)
				return GD.Load<T>(path).NotNull();
		}

		public static T LoadScene<T>(string path) where T : Node
		{
			lock (loadingLock)
			{
				var scene = GD.Load<PackedScene>(path).NotNull();
				return (T)scene.Instance();
			}
		}
	}
}
