using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using PhantomForce.Common;
using Object = Godot.Object;

namespace PhantomForce.GodotBridge
{
	class Signals : IDisposable
	{
		readonly Dictionary<int, Action> handlers = new Dictionary<int, Action>();
		readonly List<(WeakReference<Object>, int id)> producers = new List<(WeakReference<Object>, int)>();
		readonly CancellationTokenSource cleanupCancellationTokenSource = new CancellationTokenSource();

		int id = int.MinValue;

		public Signals()
		{
			Task.Run(() => Cleanup(cleanupCancellationTokenSource.Token), cleanupCancellationTokenSource.Token)
				.LogErrors();
		}

		public void Connect(RootNode root, Object source, string signal, Action handler)
		{
			var id = Interlocked.Increment(ref this.id);
			lock (producers)
				producers.Add((new WeakReference<Object>(source), id));
			lock (handlers)
				handlers[id] = handler;
			source.Connect(signal, root, "HandleSignal", new Godot.Collections.Array(id));
		}

		public void Handle(int id)
		{
			if (handlers.TryGetValue(id, out Action action))
				action();
		}

		void Cleanup(CancellationToken cancellationToken)
		{
			while (!cancellationToken.IsCancellationRequested)
			{
				lock (producers)
				{
					foreach ((var weakRef, int id) in producers)
					{
						if (!weakRef.TryGetTarget(out Object objekt))
						{
							lock (handlers)
								handlers.Remove(id);
						}
					}
				}
				Task.Delay(TimeSpan.FromMinutes(1), cancellationToken).Wait();
			}
		}

		public void Dispose()
		{
			cleanupCancellationTokenSource.Cancel();
		}
	}
}
