using Godot;

namespace PhantomForce.Utilities
{
	public static class VectorExtensions
	{
		public static System.Numerics.Vector3 ToNumerics(this Vector3 value) =>
			new System.Numerics.Vector3(value.x, value.y, value.z);

		public static Vector3 ToGodot(this System.Numerics.Vector3 value) =>
			new Vector3(value.X, value.Y, value.Z);
	}
}
