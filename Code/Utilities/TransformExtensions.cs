using BepuPhysics;
using Godot;

namespace PhantomForce.Utilities
{
	public static class TransformExtensions
	{
		public static Transform ToTransform(this RigidPose pose) =>
			new Transform
			(
				new Quat(pose.Orientation.X, pose.Orientation.Y, pose.Orientation.Z, pose.Orientation.W),
				pose.Position.ToGodot()
			);
	}
}