using System;
using System.Linq;
using Godot;

namespace PhantomForce.Utilities
{
	class Logger
	{
		static int longestTypeName = 0;

		string typeName;

		public Logger(Type type)
		{
			typeName = type.IsConstructedGenericType ?
				($"{type.GetGenericTypeDefinition().FullName}<" +
					$"{string.Join(", ", type.GenericTypeArguments.Select(t => t.FullName))}>") :
				type.FullName;
			if (typeName.Length > longestTypeName)
				longestTypeName = typeName.Length;
		}

		public void Info(string message) => GD.Print(Embellish(message));
		public void Error(string message) => GD.PrintErr(Embellish(message));
		public void Error(Exception exception) => GD.PrintErr(exception);

		string Embellish(string message)
		{
			if (typeName.Length < longestTypeName)
				typeName += new string('.', longestTypeName - typeName.Length);
			return $"[{typeName}]: {message}";
		}
	}
}
