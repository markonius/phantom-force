using System.Runtime.CompilerServices;
using BepuPhysics;
using BepuPhysics.Collidables;
using BepuPhysics.CollisionDetection;
using BepuPhysics.Constraints;

namespace PhantomForce.Physics
{
	public unsafe struct NarrowPhaseCallbacks : INarrowPhaseCallbacks
	{
		public void Initialize(BepuPhysics.Simulation simulation) { }

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public bool AllowContactGeneration(int workerIndex, CollidableReference a, CollidableReference b) =>
			// Make sure at least one of the two bodies is dynamic.
			a.Mobility == CollidableMobility.Dynamic || b.Mobility == CollidableMobility.Dynamic;

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public bool AllowContactGeneration(int workerIndex, CollidablePair pair, int childIndexA, int childIndexB) =>
			true;

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		unsafe bool INarrowPhaseCallbacks.ConfigureContactManifold<TManifold>(
			int workerIndex, CollidablePair pair, ref TManifold manifold, out PairMaterialProperties pairMaterial)
		{
			pairMaterial.FrictionCoefficient = 1f;
			pairMaterial.MaximumRecoveryVelocity = float.MaxValue;
			pairMaterial.SpringSettings = new SpringSettings(1000, 1);
			return true;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public bool ConfigureContactManifold(
			int workerIndex, CollidablePair pair, int childIndexA, int childIndexB, ref ConvexContactManifold manifold) =>
			true;

		public void Dispose() { }
	}

	public struct PoseIntegratorCallbacks : IPoseIntegratorCallbacks
	{
		public const float DRAG = 0.01f;

		public AngularIntegrationMode AngularIntegrationMode => AngularIntegrationMode.Nonconserving;

		public void Initialize(BepuPhysics.Simulation simulation) { }
		public void PrepareForIntegration(float dt) { }

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void IntegrateVelocity(
			int bodyIndex, in RigidPose pose, in BodyInertia localInertia, int workerIndex, ref BodyVelocity velocity)
		{
			// Ignore kinematics
			if (localInertia.InverseMass > 0)
			{
				velocity.Linear *= 1 - DRAG;
				velocity.Angular *= 1 - DRAG;
				velocity.Linear.Y = -pose.Position.Y;
			}
		}
	}
}