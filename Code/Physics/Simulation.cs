using System;
using BepuPhysics;
using BepuPhysics.Collidables;
using BepuUtilities.Memory;
using Godot;
using PhantomForce.Utilities;

namespace PhantomForce.Physics
{
	class Simulation : IDisposable
	{
		readonly BufferPool bufferPool = new BufferPool();
		readonly SimpleThreadDispatcher threadDispatcher = new SimpleThreadDispatcher(8);
		readonly BepuPhysics.Simulation simulation;

		bool disposed;

		public Simulation()
		{
			simulation = BepuPhysics.Simulation.Create(
				bufferPool, new NarrowPhaseCallbacks(), new PoseIntegratorCallbacks(), new PositionLastTimestepper());
		}

		public BodyHandle Create(Vector3 position, float mass)
		{
			var box = new Box(2, 2, 2);
			box.ComputeInertia(mass, out BodyInertia inertia);
			return simulation.Bodies.Add
			(
				BodyDescription.CreateDynamic
				(
					position.ToNumerics(),
					inertia,
					new CollidableDescription(simulation.Shapes.Add(box), 0.1f),
					new BodyActivityDescription(0.01f)
				)
			);
		}

		public void Move(BodyHandle handle, Vector3 translation)
		{
			simulation.Bodies.GetDescription(handle, out BodyDescription description);
			description.Pose.Position = translation.ToNumerics();
			simulation.Bodies.ApplyDescription(handle, description);
		}

		public void SetVelocity(BodyHandle handle, Vector3 velocity)
		{
			simulation.Bodies.GetDescription(handle, out BodyDescription description);
			description.Velocity.Linear = velocity.ToNumerics();
			simulation.Bodies.ApplyDescription(handle, description);
		}

		public void ApplyForce(BodyHandle handle, Vector3 force)
		{
			simulation.Bodies.GetDescription(handle, out BodyDescription description);
			description.Velocity.Linear += (force * description.LocalInertia.InverseMass).ToNumerics();
			simulation.Bodies.ApplyDescription(handle, description);
		}

		public Transform GetTransform(BodyHandle handle)
		{
			simulation.Bodies.GetDescription(handle, out BodyDescription description);
			return description.Pose.ToTransform();
		}

		public void Simulate(float delta) => simulation.Timestep(delta, threadDispatcher);

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!disposed)
			{
				if (disposing)
				{
					simulation.Dispose();
					threadDispatcher.Dispose();
					bufferPool.Clear();
				}
				disposed = true;
			}
		}

		~Simulation() => Dispose(false);
	}
}
