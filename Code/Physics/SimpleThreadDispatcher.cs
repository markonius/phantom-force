using System;
using System.Threading;
using BepuUtilities;
using BepuUtilities.Memory;
using Victor.Extensions;

namespace PhantomForce.Physics
{
	public sealed class SimpleThreadDispatcher : IThreadDispatcher, IDisposable
	{
		struct Worker
		{
			public Thread Thread;
			public AutoResetEvent Signal;
		}

		readonly int threadCount;
		public int ThreadCount => threadCount;

		readonly Worker[] workers;
		readonly AutoResetEvent finished;

		readonly BufferPool[] bufferPools;

		volatile Action<int>? workerBody;
		int workerIndex;
		int completedWorkerCounter;

		volatile bool disposed;

		public SimpleThreadDispatcher(int threadCount)
		{
			this.threadCount = threadCount;
			workers = new Worker[threadCount - 1];
			for (int i = 0; i < workers.Length; ++i)
			{
				workers[i] = new Worker { Thread = new Thread(WorkerLoop), Signal = new AutoResetEvent(false) };
				workers[i].Thread.IsBackground = true;
				workers[i].Thread.Start(workers[i].Signal);
			}
			finished = new AutoResetEvent(false);
			bufferPools = new BufferPool[threadCount];
			for (int i = 0; i < bufferPools.Length; ++i)
			{
				bufferPools[i] = new BufferPool();
			}
		}

		void DispatchThread(int workerIndex)
		{
			workerBody.NotNull()(workerIndex);

			if (Interlocked.Increment(ref completedWorkerCounter) == threadCount)
			{
				finished.Set();
			}
		}


		void WorkerLoop(object? untypedSignal)
		{
			var signal = (AutoResetEvent)untypedSignal.NotNull();
			while (true)
			{
				signal.WaitOne();
				if (disposed)
					return;
				DispatchThread(Interlocked.Increment(ref workerIndex) - 1);
			}
		}

		void SignalThreads()
		{
			for (int i = 0; i < workers.Length; ++i)
			{
				workers[i].Signal.Set();
			}
		}

		public void DispatchWorkers(Action<int> workerBody)
		{
			if (this.workerBody != null)
				throw new Exception("Worker is not null");

			workerIndex = 1; //Just make the inline thread worker 0. While the other threads might start executing first, the user should never rely on the dispatch order.
			completedWorkerCounter = 0;
			this.workerBody = workerBody;
			SignalThreads();
			//Calling thread does work. No reason to spin up another worker and block this one!
			DispatchThread(0);
			_ = finished.WaitOne();
			this.workerBody = null;
		}

		public BufferPool GetThreadMemoryPool(int workerIndex)
		{
			return bufferPools[workerIndex];
		}

		public void Dispose()
		{
			if (!disposed)
			{
				disposed = true;
				SignalThreads();
				for (int i = 0; i < bufferPools.Length; ++i)
				{
					bufferPools[i].Clear();
				}
				foreach (var worker in workers)
				{
					worker.Thread.Join();
					worker.Signal.Dispose();
				}
			}
		}
	}
}
