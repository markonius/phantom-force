using System;
using System.Runtime.CompilerServices;
using System.Runtime.ExceptionServices;

namespace PhantomForce.Flows
{
	[AsyncMethodBuilder(typeof(FlowBuilder<>))]
	class Flow<T>
	{
		T result = default!; // Nullable generic params are hard
		Action? continuation;

		public bool IsCompleted { get; private set; }

		public FlowAwaiter<T> GetAwaiter() => new FlowAwaiter<T>(this);

		public T GetResult()
		{
			if (IsCompleted)
				return result;
			else
				throw new Exception("Cannot get result of incomplete flow.");
		}

		public void SetResult(T result)
		{
			this.result = result;
			IsCompleted = true;
			continuation?.Invoke();
		}

		public void OnCompleted(Action continuation)
		{
			this.continuation = continuation;
		}
	}

	interface IFlowAwaiter : ICriticalNotifyCompletion { }

	struct FlowAwaiter<T> : IFlowAwaiter
	{
		readonly Flow<T> flow;

		public bool IsCompleted => flow.IsCompleted;

		public FlowAwaiter(Flow<T> flow)
		{
			this.flow = flow;
		}

		public void UnsafeOnCompleted(Action continuation)
		{
			flow.OnCompleted(continuation);
		}

		public void OnCompleted(Action continuation)
		{
			flow.OnCompleted(continuation);
		}

		public T GetResult() => flow.GetResult();

		public void SetResult(T result)
		{
			flow.SetResult(result);
		}
	}

	class FlowBuilder<T>
	{
		readonly Flow<T> flow = new Flow<T>();
		public Flow<T> Task => flow;

		public static FlowBuilder<T> Create() => new FlowBuilder<T>();

		public void Start<TStateMachine>(ref TStateMachine stateMachine)
			 where TStateMachine : IAsyncStateMachine
		{
			stateMachine.MoveNext();
		}

		public void SetStateMachine(IAsyncStateMachine _) => throw new NotSupportedException();

		public void SetException(Exception exception)
		{
			ExceptionDispatchInfo.Capture(exception).Throw(); // Throw and preserve stack trace
		}

		public void AwaitOnCompleted<TAwaiter, TStateMachine>(ref TAwaiter _0, ref TStateMachine _1)
			 where TAwaiter : INotifyCompletion
			 where TStateMachine : IAsyncStateMachine
		{
			throw new NotSupportedException();
		}

		public void AwaitUnsafeOnCompleted<TAwaiter, TStateMachine>(ref TAwaiter awaiter, ref TStateMachine stateMachine)
			 where TAwaiter : ICriticalNotifyCompletion
			 where TStateMachine : IAsyncStateMachine
		{
			if (awaiter is IFlowStateAwaiter || awaiter is IFlowAwaiter || awaiter is IBoundTaskAwaiter)
				awaiter.OnCompleted(stateMachine.MoveNext);
			else
				throw new NotSupportedException("Unsupported awaiter in flow: " + awaiter.GetType());
		}

		public void SetResult(T result)
		{
			flow.SetResult(result);
		}
	}
}
