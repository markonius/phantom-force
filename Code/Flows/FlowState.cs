using System;
using System.Runtime.CompilerServices;
using Victor.Extensions;

namespace PhantomForce.Flows
{
	class FlowState<T>
	{
		readonly Action<FlowState<T>> enterState;
		T result = default!; // Should be nullable, but nullable generic params are hard
		Action? continuation;

		public bool IsCompleted { get; private set; }

		FlowState(Action<FlowState<T>> enterState)
		{
			this.enterState = enterState;
		}

		public static FlowState<T> Create(Action<FlowState<T>> enterState) => new FlowState<T>(enterState);

		public FlowStateAwaiter<T> GetAwaiter() => new FlowStateAwaiter<T>(this);

		public T GetResult()
		{
			if (IsCompleted)
				return result;
			else
				throw new Exception("Cannot get result of incomplete flow.");
		}

		/// <summary>
		/// Mark the state as complete and resume the containing flow.
		/// </summary>
		/// <param name="result">Result produced by this state</param>
		/// <returns>False if state has already been completed, true otherwise</returns>
		public bool Complete(T result)
		{
			if (!IsCompleted)
			{
				this.result = result;
				IsCompleted = true;
				continuation.NotNull().Invoke();
				return true;
			}
			else
			{
				return false;
			}
		}

		public void OnCompleted(Action continuation)
		{
			this.continuation = continuation;
			enterState(this);
		}
	}

	interface IFlowStateAwaiter : ICriticalNotifyCompletion { }

	struct FlowStateAwaiter<T> : IFlowStateAwaiter
	{
		readonly FlowState<T> flowState;

		public bool IsCompleted => flowState.IsCompleted;

		public FlowStateAwaiter(FlowState<T> flowState)
		{
			this.flowState = flowState;
		}

		public void UnsafeOnCompleted(Action continuation)
		{
			flowState.OnCompleted(continuation);
		}

		public void OnCompleted(Action continuation)
		{
			flowState.OnCompleted(continuation);
		}

		public T GetResult()
		{
			return flowState.GetResult();
		}
	}
}
