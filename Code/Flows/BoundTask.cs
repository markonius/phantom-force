using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using PhantomForce.Common;
using PhantomForce.GodotBridge;

namespace PhantomForce.Flows
{
	static class BoundTask
	{
		public static BoundTask<T> Run<T>(Func<T> function, IContext context)
		{
			return new BoundTask<T>(Task.Run(function).LogErrors(), context);
		}
	}

	class BoundTask<T>
	{
		readonly Task<T> task;
		readonly IContext context;
		public bool IsCompleted => task.IsCompleted;

		public BoundTask(Task<T> task, IContext context)
		{
			this.task = task;
			this.context = context;
		}

		public BoundTaskAwaiter<T> GetAwaiter() => new BoundTaskAwaiter<T>(this);

		public void OnCompleted(Action continuation)
		{
			task.ContinueWith(t => context.DeferAndForget(continuation));
		}

		public T GetResult() => task.Result;
	}

	interface IBoundTaskAwaiter : ICriticalNotifyCompletion { }

	struct BoundTaskAwaiter<T> : IBoundTaskAwaiter
	{
		readonly BoundTask<T> task;

		public bool IsCompleted => task.IsCompleted;

		public BoundTaskAwaiter(BoundTask<T> task)
		{
			this.task = task;
		}

		public void OnCompleted(Action continuation)
		{
			task.OnCompleted(continuation);
		}

		public void UnsafeOnCompleted(Action continuation)
		{
			task.OnCompleted(continuation);
		}

		public T GetResult() => task.GetResult();
	}
}
