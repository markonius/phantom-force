using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Godot;
using PhantomForce.Common;
using PhantomForce.Common.Network;
using PhantomForce.GodotBridge;
using PhantomForce.Server.Network;
using PhantomForce.Utilities;
using Victor.Yves;
using Array = Godot.Collections.Array;

namespace PhantomForce.Server.GameScene
{
	class Game : IEventHandler
	{
		static readonly Logger logger = new Logger(typeof(Game));

		readonly Queue<Vector2[]> worldStateBufferPool = new Queue<Vector2[]>();

		readonly ServerContext context;
		readonly LiteNetServer<InputMessage> inputServer;
		readonly LiteNetServer<WorldStateMessage> worldStateServer;
		readonly Spatial gameNode;
		readonly List<Player> players = new List<Player>();

		SceneTask<None>? sceneTask;

		Game(ServerContext context, Spatial gameNode, List<int> playerIDs)
		{
			for (int i = 0; i < 20; i++)
				worldStateBufferPool.Enqueue(new Vector2[playerIDs.Count]);

			this.context = context;
			this.gameNode = gameNode;
			inputServer = context.root.inputServer;
			worldStateServer = context.root.worldStateServer;

			Array spawnPoints = gameNode.GetNode("SpawnPoints").GetChildren();
			var playerScene = Loader.Load<PackedScene>("res://Assets/Server/Game/Player.tscn");
			foreach (int id in playerIDs)
			{
				var playerNode = playerScene.Instance<Spatial>();
				var spawnPoint = (Spatial)spawnPoints[id];
				context.Defer(() =>
				{
					gameNode.AddChild(playerNode);
					playerNode.Translation = spawnPoint.Translation;
				}).Wait();
				var player = new Player(playerNode, id);
				players.Add(player);
			}
		}

		public static None Run(ServerContext context, Spatial gameNode, List<int> playerIDs)
		{
			var game = new Game(context, gameNode, playerIDs);
			Task.Run(game.SendStart).LogErrors();
			context.DeferState<None>(game.StartGameLoop).Wait();
			context.RemoveEventHandler(game);
			return None.none;
		}

		void SendStart()
		{
			Task.Delay(TimeSpan.FromSeconds(0.15)).Wait(); // Give chance to clients to setup handlers.
			foreach (TCPPeer peer in context.root.tcpServer.Peers.Values)
				peer.Send("Start", players.Count);
		}

		void StartGameLoop(SceneTask<None> task)
		{
			sceneTask = task;
			context.AddEventHandler(this);
			logger.Info("Started server game loop.");
		}

		public void FrameUpdate(float delta) { }

		public void Step(float delta)
		{
			SendWorldState();
			ApplyInputs();
			// TODO: simulate
		}

		private void ApplyInputs()
		{
			bool success;
			do
			{
				InputMessage input;
				(success, input) = inputServer.TryReceive();
				if (success)
					// TODO: move to simulate step
					players[input.playerID].Position += input.direction;
			} while (success);
		}

		void SendWorldState()
		{
			Vector2[] buffer = worldStateBufferPool.Dequeue();
			for (int i = 0; i < buffer.Length; i++)
				buffer[i] = players[i].Position;
			var state = new WorldStateMessage(buffer);
			worldStateServer.EnqueueBroadcast(state);
			worldStateBufferPool.Enqueue(buffer);
		}

		public void Input(InputEvent e) { }
	}
}
