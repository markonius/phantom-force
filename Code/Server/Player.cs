using Godot;

namespace PhantomForce.Server
{
	class Player
	{
		public readonly int id;
		readonly Spatial playerNode;

		public Vector2 Position
		{
			get
			{
				Vector3 position = playerNode.Translation;
				return new Vector2(position.x, position.z);
			}
			set
			{
				Vector3 position = new Vector3(value.x, 0, value.y);
				playerNode.Translation = position;
			}
		}

		public Player(Spatial playerNode, int id)
		{
			this.playerNode = playerNode;
			this.id = id;
		}

	}
}
