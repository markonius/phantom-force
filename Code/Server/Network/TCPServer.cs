using System;
using System.Collections.Generic;
using System.Text;
using PhantomForce.Common.Network;
using PhantomForce.Utilities;
using WatsonTcp;

namespace PhantomForce.Server.Network
{
	class TCPServer : IDisposable
	{
		readonly static Logger logger = new Logger(typeof(TCPServer));

		public const int PORT = 35622;
		readonly WatsonTcpServer server;
		readonly Dictionary<string, TCPPeer> peers = new Dictionary<string, TCPPeer>();

		public IReadOnlyDictionary<string, TCPPeer> Peers => peers;
		public event Action<TCPPeer>? PeerConnected;

		public TCPServer()
		{
			server = new WatsonTcpServer("0.0.0.0", PORT);
			server.Events.ClientConnected += ClientConnected;
			server.Events.ClientDisconnected += ClientDisconnected;
			server.Events.MessageReceived += MessageReceived;
			server.Callbacks.SyncRequestReceived = SyncRequestReceived;
		}

		public void Start()
		{
			server.Start();
			logger.Info("Started TCP server");
		}

		void ClientConnected(object sender, ConnectionEventArgs args)
		{
			logger.Info("Client connected: " + args.IpPort);
			var peer = new TCPPeer((l, s) => server.Send(args.IpPort, l, s));
			peers[args.IpPort] = peer;
			try
			{
				PeerConnected?.Invoke(peer);
			}
			catch (Exception e)
			{
				logger.Error(e);
			}
		}

		void ClientDisconnected(object sender, DisconnectionEventArgs args)
		{
			logger.Info("Client disconnected: " + args.IpPort + ": " + args.Reason.ToString());
			peers.Remove(args.IpPort);
		}

		void MessageReceived(object sender, MessageReceivedEventArgs args)
		{
			logger.Info("Message from " + args.IpPort);
			try
			{
				peers[args.IpPort].Receive(args.Data);
			}
			catch (Exception e)
			{
				logger.Error(e);
			}
		}

		static SyncResponse SyncRequestReceived(SyncRequest req)
		{
			logger.Info($"Sync request: {Encoding.UTF8.GetString(req.Data)}");
			return new SyncResponse(req, "pong");
		}

		public void Dispose()
		{
			server.DisconnectClients(MessageStatus.Shutdown);
			peers.Clear();
			server.Dispose();
		}
	}
}
