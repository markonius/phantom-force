using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using LiteNetLib;
using PhantomForce.GodotBridge;
using PhantomForce.Utilities;
using ProtoBuf;
using Victor.Extensions;
using Thread = System.Threading.Thread;

namespace PhantomForce.Server.Network
{
	static class LiteNetServer
	{
		public const int INPUT_PORT = 35623;
		public const int WORLD_STATE_PORT = 35624;
	}

	class LiteNetServer<T> : IDisposable where T : notnull
	{
		readonly Logger logger = new Logger(typeof(LiteNetServer<T>));

		static readonly ThreadLocal<byte[]> buffer = new ThreadLocal<byte[]>(() => new byte[1024]);

		NetManager? server;
		readonly Queue<T> inbox = new Queue<T>();
		readonly Queue<T> outbox = new Queue<T>();
		readonly ManualResetEvent outboxAddEvent = new ManualResetEvent(false);

		bool disposed;

		public void Start(int port)
		{
			Task.Run(() => Listen(port)).LogErrors();
			Task.Run(Dispatch).LogErrors();
		}

		void Listen(int port)
		{
			EventBasedNetListener listener = new EventBasedNetListener();
			server = new NetManager(listener);
			server.Start(port);

			logger.Info("Started UDP server.");

			listener.ConnectionRequestEvent += request =>
			{
				request.AcceptIfKey("Hello");
			};

			listener.PeerConnectedEvent += peer =>
			{
				logger.Info($"Client connected: {peer.EndPoint}");
			};

			listener.PeerDisconnectedEvent += (peer, info) =>
			{
				logger.Info($"Client disconnected: {peer.EndPoint} ({info.Reason})");
			};

			listener.NetworkReceiveEvent += Receive;

			while (!disposed)
			{
				server.PollEvents();
				Thread.Sleep(8);
			}

			server.Stop();
			server = null;
		}

		void Receive(NetPeer peer, NetPacketReader reader, DeliveryMethod deliveryMethod)
		{
			T message;
			using (var stream = new MemoryStream(reader.RawData))
			{
				stream.Position = reader.UserDataOffset;
				message = Serializer.DeserializeWithLengthPrefix<T>(stream, PrefixStyle.Base128);
			}
			reader.Recycle();
			lock (inbox)
				inbox.Enqueue(message);
		}

		void Dispatch()
		{
			while (!disposed)
			{
				bool hasSome;
				T message;
				lock (outbox)
					hasSome = outbox.TryDequeue(out message);
				if (hasSome)
				{
					Broadcast(message);
				}
				else
				{
					outboxAddEvent.WaitOne();
					outboxAddEvent.Reset();
				}
			}
		}

		public void Broadcast(T message)
		{
			byte[] buffer = LiteNetServer<T>.buffer.Value;
			long length;
			using (var stream = new MemoryStream(buffer, true))
			{
				stream.SetLength(0);
				stream.Position = 0;
				Serializer.SerializeWithLengthPrefix(stream, message, PrefixStyle.Base128);
				length = stream.Length;
			}
			server.NotNull().SendToAll(buffer, 0, (int)length, DeliveryMethod.Unreliable);
		}

		public void EnqueueBroadcast(T message)
		{
			lock (outbox)
				outbox.Enqueue(message);
			outboxAddEvent.Set();
		}

		public (bool success, T value) TryReceive()
		{
			lock (inbox)
			{
				if (inbox.TryDequeue(out T result))
					return (true, result);
				else
					return (false, default)!;
			}
		}

		public void Dispose()
		{
			disposed = true;
		}
	}
}
