using System.Collections.Generic;
using System.Threading;
using PhantomForce.Common.Network;
using PhantomForce.Utilities;
using Victor.Yves;

namespace PhantomForce.Server
{
	static class Lobby
	{
		readonly static Logger logger = new Logger(typeof(Lobby));

		public const int PLAYER_COUNT = 2;

		public static List<int> Run(ServerContext context)
		{
			var playerIDs = new List<int>();
			AutoResetEvent leLock = new AutoResetEvent(false);
			int id = 0;

			void OnPlayerConnected(TCPPeer peer)
			{
				int pID = id++;
				playerIDs.Add(pID);
				logger.Info($"Player {pID} connected.");
				peer.Send("AssignID", pID);
				if (id >= PLAYER_COUNT)
				{
					foreach (TCPPeer peer1 in context.root.tcpServer.Peers.Values)
						peer1.RemoveHandler("Connect");
					context.root.tcpServer.PeerConnected -= AddHandler;
					leLock.Set();
				}
			};

			void AddHandler(TCPPeer peer)
			{
				peer.RegisterHandler<None>("Connect", _ => OnPlayerConnected(peer));
			}

			context.root.tcpServer.PeerConnected += AddHandler;
			logger.Info("Waiting for players...");
			leLock.WaitOne();
			return playerIDs;
		}
	}
}
