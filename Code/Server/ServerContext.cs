using System;
using PhantomForce.Common;
using PhantomForce.Flows;
using PhantomForce.GodotBridge;
using Victor.Yves;
using Object = Godot.Object;

namespace PhantomForce.Server
{
	class ServerContext : IContext
	{
		public readonly ServerRoot root;
		public RootNode Root => root;

		public ServerContext(ServerRoot root)
		{
			this.root = root;
		}

		public void Connect(Object source, string signal, Action handler)
		{
			root.signals.Connect(root, source, signal, handler);
		}

		public void DeferAndForget(Action action)
		{
			root.deferredActions.DeferAndForget(action);
		}

		public SceneTask<T> DeferState<T>(Action<SceneTask<T>> enterState) => root.deferredActions.DeferState(enterState);
		public SceneTask<T> Defer<T>(Func<T> function) => root.deferredActions.Defer(function);
		public SceneTask<None> Defer(Action action) => root.deferredActions.Defer(action);
		public SceneTask<T> DeferFlow<T>(Func<Flow<T>> flow) => root.deferredActions.DeferFlow(flow);

		public void AddEventHandler(IEventHandler eventHandler)
		{
			root.AddEventHandler(eventHandler);
		}

		public void RemoveEventHandler(IEventHandler eventHandler)
		{
			root.RemoveEventHandler(eventHandler);
		}
	}
}
