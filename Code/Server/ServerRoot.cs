using System.Collections.Generic;
using Godot;
using PhantomForce.Common;
using PhantomForce.Common.Network;
using PhantomForce.Server.GameScene;
using PhantomForce.Server.Network;
using Victor.Yves;

namespace PhantomForce.Server
{
	class ServerRoot : RootNode
	{
		readonly ServerContext context;
		public override IContext Context => context;
		public readonly TCPServer tcpServer = new TCPServer();
		public readonly LiteNetServer<InputMessage> inputServer = new LiteNetServer<InputMessage>();
		public readonly LiteNetServer<WorldStateMessage> worldStateServer = new LiteNetServer<WorldStateMessage>();

		public ServerRoot()
		{
			context = new ServerContext(this);
		}

		protected override void Start()
		{
			tcpServer.Start();
			inputServer.Start(LiteNetServer.INPUT_PORT);
			worldStateServer.Start(LiteNetServer.WORLD_STATE_PORT);

			var playerIDs = Lobby.Run(context);
			RunGameScene(playerIDs);
			Free();
			Dispose();
		}

		void RunGameScene(List<int> players)
		{
			RunScene<Spatial, None>(
				"res://Assets/Server/Game.tscn",
				game => Game.Run(context, game, players)
			);
		}

		protected override void Dispose(bool disposing)
		{
			tcpServer.Dispose();
			base.Dispose(disposing);
		}
	}
}
