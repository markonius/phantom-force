using System.Threading;
using Godot;
using PhantomForce.Flows;
using PhantomForce.GodotBridge;
using PhantomForce.Server.Network;
using Victor.Extensions;
using Victor.Yves;

namespace PhantomForce.Client
{
	static class ConnectPrompt
	{
		public static void Run(SceneTask<int> task, ClientContext context, Control connectPrompt)
		{
			Button connectButton = connectPrompt.GetNode<Button>("Background/ConnectButton");
			context.Connect(connectButton, "pressed", () => ConnectButtonPressed().Ignore());

			async Flow<None> ConnectButtonPressed()
			{
				string buttonText = connectButton.Text;
				connectButton.Text = "Connecting...";
				connectButton.Disabled = true;
				int? id = await BoundTask.Run(() => ConnectToServer(context), context);
				if (id.HasValue)
				{
					task.Complete(id.Value);
				}
				else
				{
					connectButton.Text = buttonText;
					connectButton.Disabled = false;
				}
				return None.none;
			}
		}

		static int? ConnectToServer(ClientContext context)
		{
			context.root.tcpClient.ConnectToServer();
			context.root.inputClient.ConnectToServer(LiteNetServer.INPUT_PORT);
			context.root.worldStateClient.ConnectToServer(LiteNetServer.WORLD_STATE_PORT);

			int? id = null;
			var leLock = new AutoResetEvent(false);
			context.root.tcpClient.Server.RegisterHandler<int>("AssignID", id1 =>
			{
				id = id1;
				leLock.Set();
			});
			context.root.tcpClient.Server.Send("Connect", None.none);
			leLock.WaitOne();
			return id;
		}
	}
}
