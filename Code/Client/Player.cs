using Godot;
using PhantomForce.Common.Network;

namespace PhantomForce.Client
{
	class Player
	{
		const float MOUSE_SENSITIVITY = 0.002f;
		const float MOVEMENT_SPEED = 1f;

		public readonly int playerID;
		readonly Spatial playerNode;
		readonly Camera camera;

		Vector2 mouseDelta;

		public Player(Spatial playerNode, int playerID)
		{
			this.playerNode = playerNode;
			this.playerID = playerID;
			camera = playerNode.GetNode<Camera>("Camera");
		}

		public void ReadMouseInput(InputEvent @event)
		{
			if (@event is InputEventMouseMotion motionEvent)
				mouseDelta = -motionEvent.Relative * MOUSE_SENSITIVITY;
		}

		public InputMessage GetInput(float delta)
		{
			var translation = new Vector2
			(
				(Input.IsActionPressed("move_right") ? 1 : 0) - (Input.IsActionPressed("move_left") ? 1 : 0),
				-(Input.IsActionPressed("move_forward") ? 1 : 0) + (Input.IsActionPressed("move_backward") ? 1 : 0)
			) * delta * MOVEMENT_SPEED;
			return new InputMessage((byte)playerID, translation);
		}

		internal void SetPosition(Vector2 position)
		{
			playerNode.Translation = new Vector3(position.x, 0, position.y);
		}
	}
}
