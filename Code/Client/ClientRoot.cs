using Godot;
using PhantomForce.Client.GameScene;
using PhantomForce.Client.LobbyScene;
using PhantomForce.Client.Network;
using PhantomForce.Common;
using PhantomForce.Common.Network;
using PhantomForce.Utilities;
using Victor.Yves;

namespace PhantomForce.Client
{
	class ClientRoot : RootNode
	{
		readonly static Logger logger = new Logger(typeof(ClientRoot));

		readonly ClientContext context;
		public override IContext Context => context;

		public readonly TCPClient tcpClient = new TCPClient();
		public readonly LiteNetClient<InputMessage> inputClient = new LiteNetClient<InputMessage>();
		public readonly LiteNetClient<WorldStateMessage> worldStateClient = new LiteNetClient<WorldStateMessage>();

		public ClientRoot()
		{
			context = new ClientContext(this);
		}

		protected override void Start()
		{
			int id = RunConnectPromptsScene();
			int playerCount = RunLobbyScene();
			logger.Info($"Got id {id} and player count {playerCount}");
			RunGameScene(id, playerCount);
			Free();
			Dispose();
		}

		/// <returns>Player ID</returns>
		int RunConnectPromptsScene() =>
			RunScene<Control, int>(
				"res://Assets/Client/ConnectPrompt.tscn",
				connectPrompt => context.DeferState<int>(t => ConnectPrompt.Run(t, context, connectPrompt)).Wait()
			);

		/// <returns>Player count</returns>
		int RunLobbyScene() =>
			RunScene<Control, int>(
				"res://Assets/Client/Lobby.tscn",
				lobby => Lobby.WaitForStartAndPlayerCount(context)
			);

		void RunGameScene(int playerID, int playerCount)
		{
			RunScene<Spatial, None>(
				"res://Assets/Client/Game.tscn",
				game => Game.Run(context, game, playerID, playerCount)
			);
		}

		protected override void Dispose(bool disposing)
		{
			tcpClient.Dispose();
			base.Dispose(disposing);
		}
	}
}
