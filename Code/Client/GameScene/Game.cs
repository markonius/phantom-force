using Godot;
using PhantomForce.Client.Network;
using PhantomForce.Common;
using PhantomForce.Common.Network;
using PhantomForce.GodotBridge;
using PhantomForce.Utilities;
using Victor.Yves;
using Array = Godot.Collections.Array;

namespace PhantomForce.Client.GameScene
{
	class Game : IEventHandler
	{
		static readonly Logger logger = new Logger(typeof(Game));

		readonly ClientContext context;
		readonly LiteNetClient<InputMessage> inputClient;
		readonly LiteNetClient<WorldStateMessage> worldStateClient;
		readonly Spatial gameNode;
		readonly Player player;

		SceneTask<None>? sceneTask;

		Game(ClientContext context, Spatial gameNode, int playerID, int playerCount)
		{
			this.context = context;
			this.gameNode = gameNode;
			inputClient = context.root.inputClient;
			worldStateClient = context.root.worldStateClient;

			var playerNode = Loader.LoadScene<Spatial>("res://Assets/Client/Game/Player.tscn");
			player = new Player(playerNode, playerID);

			Array spawnPoints = gameNode.GetNode("SpawnPoints").GetChildren();
			var spawnPoint = (Spatial)spawnPoints[playerID];
			context.Defer(() =>
			{
				gameNode.AddChild(playerNode);
				playerNode.Translation = spawnPoint.Translation;
			}).Wait();
		}

		public static None Run(ClientContext context, Spatial gameNode, int playerID, int playerCount)
		{
			var game = new Game(context, gameNode, playerID, playerCount);
			context.DeferState<None>(game.StartGameLoop).Wait();
			context.RemoveEventHandler(game);
			return None.none;
		}

		void StartGameLoop(SceneTask<None> task)
		{
			sceneTask = task;
			context.AddEventHandler(this);
			logger.Info("Started client game loop.");
		}

		public void FrameUpdate(float delta) { }

		public void Step(float delta)
		{
			SendInputs(delta);
			// TODO: rotate player
			ApplyWorldState();
		}

		void SendInputs(float delta)
		{
			InputMessage input = player.GetInput(delta);
			if (input.direction != Vector2.Zero)
				inputClient.EnqueueSend(input);
		}

		void ApplyWorldState()
		{
			bool success;
			WorldStateMessage? state = null;
			do
			{
				WorldStateMessage tempState;
				(success, tempState) = worldStateClient.TryReceive();
				if (success)
					state = tempState;
			} while (success);
			if (state != null)
				player.SetPosition(state.Value.positions[player.playerID]);
		}

		public void Input(InputEvent e)
		{
			player.ReadMouseInput(e);
		}
	}
}
