using System.Threading;

namespace PhantomForce.Client.LobbyScene
{
	static class Lobby
	{
		public static int WaitForStartAndPlayerCount(ClientContext context)
		{
			int playerCount = -1;
			var leLock = new AutoResetEvent(false);
			context.root.tcpClient.Server.RegisterHandler<int>("Start", pc =>
			{
				playerCount = pc;
				context.root.tcpClient.Server.RemoveHandler("Start");
				leLock.Set();
			});
			leLock.WaitOne();
			return playerCount;
		}
	}
}
