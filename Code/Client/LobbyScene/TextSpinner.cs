using Godot;

namespace PhantomForce.Client.LobbyScene
{
	/// <summary>
	/// So we know UI isn't blocked
	/// </summary>
	class TextSpinner : Panel
	{
		[Export] public float speed = 1;

		public override void _Process(float delta)
		{
			RectRotation = (RectRotation + (delta * speed)) % 360;
		}
	}
}
