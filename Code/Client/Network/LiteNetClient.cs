using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using LiteNetLib;
using PhantomForce.GodotBridge;
using PhantomForce.Utilities;
using ProtoBuf;
using Victor.Extensions;
using static Victor.ExpressionKeywords;
using Thread = System.Threading.Thread;

namespace PhantomForce.Client.Network
{
	class LiteNetClient<T> : IDisposable where T : notnull
	{
		static readonly Logger logger = new Logger(typeof(LiteNetClient<T>));

		static readonly ThreadLocal<byte[]> buffer = new ThreadLocal<byte[]>(() => new byte[1024]);

		NetManager? client;
		readonly Queue<T> inbox = new Queue<T>();
		readonly Queue<T> outbox = new Queue<T>();
		readonly ManualResetEvent outboxAddEvent = new ManualResetEvent(false);

		bool disposed;

		public void ConnectToServer(int port)
		{
			Task.Run(() => Listen(port)).LogErrors();
			Task.Run(Dispatch).LogErrors();
		}

		void Listen(int port)
		{
			EventBasedNetListener listener = new EventBasedNetListener();
			client = new NetManager(listener);
			client.Start();
			client.Connect("localhost", port, "Hello");

			logger.Info("Started UDP client.");

			listener.PeerConnectedEvent += peer =>
			{
				logger.Info($"Connected to server: {peer.EndPoint}");
			};

			listener.NetworkReceiveEvent += Receive;

			while (!disposed)
			{
				client.PollEvents();
				Thread.Sleep(8);
			}

			client.Stop();
			client = null;
		}

		void Receive(NetPeer peer, NetPacketReader reader, DeliveryMethod deliveryMethod)
		{
			T message;
			using (var stream = new MemoryStream(reader.RawData))
			{
				stream.Position = reader.UserDataOffset;
				message = Serializer.DeserializeWithLengthPrefix<T>(stream, PrefixStyle.Base128);
			}
			reader.Recycle();
			lock (inbox)
				inbox.Enqueue(message);
		}

		void Dispatch()
		{
			while (!disposed)
			{
				bool hasSome;
				T message;
				lock (outbox)
					hasSome = outbox.TryDequeue(out message);
				if (hasSome)
				{
					Send(message);
				}
				else
				{
					outboxAddEvent.WaitOne();
					outboxAddEvent.Reset();
				}
			}
		}

		public void Send(T message)
		{
			byte[] buffer = LiteNetClient<T>.buffer.Value;
			long length = Using(new MemoryStream(buffer, true), stream =>
			{
				stream.SetLength(0);
				stream.Position = 0;
				Serializer.SerializeWithLengthPrefix(stream, message, PrefixStyle.Base128);
				return stream.Length;
			});
			client.NotNull().SendToAll(buffer, 0, (int)length, DeliveryMethod.Unreliable);
		}

		public void EnqueueSend(T message)
		{
			lock (outbox)
				outbox.Enqueue(message);
			outboxAddEvent.Set();
		}

		public (bool success, T value) TryReceive()
		{
			lock (inbox)
			{
				if (inbox.TryDequeue(out T result))
					return (true, result);
				else
					return (false, default)!;
			}
		}

		public void Dispose()
		{
			disposed = true;
		}
	}
}
