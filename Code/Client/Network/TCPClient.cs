using System;
using System.Text;
using PhantomForce.Common.Network;
using PhantomForce.Server.Network;
using PhantomForce.Utilities;
using Victor.Extensions;
using WatsonTcp;

namespace PhantomForce.Client.Network
{
	class TCPClient : IDisposable
	{
		static readonly Logger logger = new Logger(typeof(TCPClient));

		readonly WatsonTcpClient client;
		TCPPeer? server;
		public TCPPeer Server => server.NotNull();

		public TCPClient()
		{
			client = new WatsonTcpClient("127.0.0.1", TCPServer.PORT);
			client.Events.ServerConnected += ServerConnected;
			client.Events.ServerDisconnected += ServerDisconnected;
			client.Events.MessageReceived += MessageReceived;
			client.Callbacks.SyncRequestReceived = SyncRequestReceived;
		}

		public void ConnectToServer()
		{
			client.Connect();
			server = new TCPPeer((l, s) => client.Send(l, s));
		}

		public void Disconnect()
		{
			server = null;
			client.Disconnect();
		}

		static void ServerConnected(object sender, ConnectionEventArgs args)
		{
			logger.Info("Server " + args.IpPort + " connected");
		}

		void ServerDisconnected(object sender, DisconnectionEventArgs args)
		{
			logger.Info("Server " + args.IpPort + " disconnected");
			server = null;
		}

		void MessageReceived(object sender, MessageReceivedEventArgs args)
		{
			logger.Info("Message from " + args.IpPort);
			try
			{
				server!.Receive(args.Data);
			}
			catch (Exception e)
			{
				logger.Error(e);
			}
		}

		static SyncResponse SyncRequestReceived(SyncRequest req)
		{
			logger.Info($"Sync request: {Encoding.UTF8.GetString(req.Data)}");
			return new SyncResponse(req, "pong");
		}

		public void Dispose()
		{
			server = null;
			client.Dispose();
		}
	}
}
